import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Window {
    width: 640
    height: 480
    visible: true
    title: "Let's do some pointless calculations!"

    function doNothing(max) {
        for(var i=0;i<max;i++) {
            for(var x=0;x<max;x++) {
                for(var y=0;y<max;y++) {

                }
            }
        }
        return max;
    }

    WorkerScript {
          id: myWorker
          source: "bigCalc.js"
          onMessage: (messageObject)=> {
                         resultBox.text += "Done with "+pBox.value+"\n"
                         busyInd.running = false;
                     }
      }

    RowLayout {
        id: rowLayoutItem
        anchors.centerIn: parent

        SpinBox {
            id: pBox
            from: 1
            to: 99999
            value: 1000
            stepSize: 1000
        }

        CheckBox {
            id: goMulti
            text: "Go Multithreaded"
            checked: true
        }


        Button {
            text: "Go calculate stuff!";
            onClicked: {
                busyInd.running = true;
                resultBox.text += "Going to start calculating\n"
                if(goMulti.checked) {
                    myWorker.sendMessage(pBox.value);
                }
                else {
                    doNothing(pBox.value);
                    resultBox.text += "Done with "+pBox.value+"\n"
                    busyInd.running = false;
                }
            }
        }

        BusyIndicator {
            id: busyInd
            running: false
        }
    }

    TextArea {
        id: resultBox
        anchors.left: rowLayoutItem.left
        anchors.right:  rowLayoutItem.right
        anchors.top:  rowLayoutItem.bottom
        height: 100
        readOnly: true
    }
}
